# probabilistic-computing-machine
This project is a Python 🐍 implementation of concepts 
  presented in a book titled Probabilistic Robotics 🧠🤖
  by Sebastian Thrun, Wolfram Burgard, and Dieter Fox.
I am not completely certain where this will lead,
  but we shall see.
